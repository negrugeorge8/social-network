package controller;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import socialnetwork.domain.Message;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.MessageService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.RequestService;
import socialnetwork.service.UtilizatorService;

import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public class PDFController {
    @FXML
    private TextField prieten;
    @FXML
    private TextField data_prieten;
    @FXML
    private TextField data_activitate;



    private UtilizatorService srv;
    private PrietenieService srvPrietenie;
    private MessageService srvMessaj;
    private RequestService srvRquest;
    private Long idUserLongged;

    public void initial(Long idUserLongged , UtilizatorService srvU, PrietenieService srvP, MessageService srvMessaj, RequestService srvRequest) {
        this.idUserLongged=idUserLongged;
        this.srv = srvU;
        this.srvPrietenie = srvP;
        this.srvMessaj = srvMessaj;
        this.srvRquest = srvRequest;
        refresh();
    }

    public void refresh() {
        System.out.println("da");
    }


    public void mesaje_pdf(MouseEvent mouseEvent) {
        String luna = data_prieten.getText().toString();
        try{
            Month l = Month.valueOf(luna.toUpperCase());

            if(prieten.getText().toString() =="")
            {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("id user invalid");
                alert.setContentText("enter a valid username");
                alert.showAndWait();
            }
            else if(srv.getOne(Long.parseLong(prieten.getText().toString()))==null)
            {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("id-ul nu se gaseste in baza de date");
                alert.setContentText("incercati alt id... nu am putut sa il gasim in baza de date");
                alert.showAndWait();
            }
            else{
                Utilizator pr = srv.getOne(Long.parseLong(prieten.getText().toString()));
                List<Message> lista_mesaje = srvMessaj.messaje_luna_user(idUserLongged,pr.getId(),luna);

                try {
                    com.itextpdf.text.Document document = new Document();

                    PdfWriter.getInstance(document, new FileOutputStream("mesaje_pdf.pdf"));

                    document.open();
                    Paragraph paragraph = new Paragraph("Messaje de la un user: " +pr.getFirstName() + " pentru userul cu id: " + idUserLongged.toString() );
                    document.add(paragraph);

                    document.add(new Paragraph("\n"));

                    lista_mesaje.forEach(x->{

                        try{
                           document.add(new Paragraph("mesaj: " + x.get_message() + "    data: " + x.getData() ));
                        }
                        catch(Exception e)
                        {
                            e.printStackTrace();
                        }

                    });


                    document.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }



        }
        catch(Exception e)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("luna invalida");
            alert.setContentText("introduceti o luna din cele 12...");
            alert.showAndWait();
        }

    }

    public void activitate_pdf(MouseEvent mouseEvent) {
        String luna = data_activitate.getText().toString();

        try{
            Month l = Month.valueOf(luna.toUpperCase());
            ArrayList<Tuple<Utilizator,String>> prieteni_data = new ArrayList<Tuple<Utilizator,String>>();



            prieteni_data = (ArrayList<Tuple<Utilizator, String>>) srvPrietenie.get_friends_list_by_month1(idUserLongged,luna);

            prieteni_data.forEach(x-> System.out.println(x.getLeft() + "  " + x.getRight()));

            List<Tuple<Message, String>> mesaje_luna =  srvMessaj.mesaje_luna(idUserLongged,luna);

            System.out.println("<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            System.out.println(idUserLongged);
            System.out.println("<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            mesaje_luna.forEach(x-> System.out.println(x.getRight()));

            // am facut rost de prietenii si de mesajele pe care le a avut ficare user
            // acum trebuie sa creem pdf urile

            try {
                com.itextpdf.text.Document document = new Document();

                PdfWriter.getInstance(document, new FileOutputStream("activitate_pdf.pdf"));

                document.open();
                Paragraph paragraph = new Paragraph("NEW FRIENDSHIPS FOR THE USER IN " + luna + ": ");
                document.add(paragraph);

                // add prieteni to pdf
                prieteni_data.forEach(x->{
                   try{
                       document.add(new Paragraph(x.getLeft().getFirstName() +"  " + x.getLeft().getLastName() + "   "+ x.getLeft().getEmail()));
                    }
                   catch(Exception e)
                   {
                       e.printStackTrace();
                   }
                });
                //
                document.add(new Paragraph("\n"));
                document.add(new Paragraph("\n"));
                document.add(new Paragraph("Messages for the user with id " + idUserLongged.toString() + ": "));

                //add mesaje to pdf

                mesaje_luna.forEach(x->{
                    try{
                        document.add(new Paragraph(x.getRight() + "  "+x.getLeft().get_message()+ "       la data de:  " + x.getLeft().getData()));
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }

                });

                document.close();
            } catch (Exception e) {
                e.printStackTrace();
            }



        }catch(Exception e)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("luna invalida");
            alert.setContentText("introduceti o luna din cele 12...");
            alert.showAndWait();
        }

    }
}
